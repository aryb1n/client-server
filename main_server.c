#include "server.h"

int main(int argc, char *argv[])
{
	broadcast_arg.number_of_message = 0;
	
	if((broadcast_arg.msgid  = msgget((key_t)1234,
										0666 | IPC_CREAT)) == -1)
		DieWithError("msgget failed with error:");

	
    /*Thread id from TCP*/		
	pthread_t threadID_listner_recv, threadID_listner_send;
	
	int mark_reciver = 0;/*Thread from recive message on tcp*/
    if (pthread_create(&threadID_listner_recv, NULL, Thread_listner,
						&mark_reciver) != 0) 
		DieWithError("pthread_create() failed");
		
	int mark_sender = 1;/*Thread from send message on tcp*/
    if (pthread_create(&threadID_listner_send, NULL, Thread_listner,
						&mark_sender) != 0) 
		DieWithError("pthread_create() failed");		
		
	pthread_t threadID_udp1, threadID_udp2;   /* Thread ID from UDP */
	int broadcast_port_send[NUM_PORTS_UDP+1]; /*Broadcast ports*/
	int broadcast_port_recv[NUM_PORTS_UDP+1]; 
	broadcast_port_send[0] = B_PORT_FOR_SENDER;
	broadcast_port_recv[0] = B_PORT_FOR_RECIVER;
	/*Start UDP threads*/
	for(int i = 0; i < NUM_PORTS_UDP; i++)
	{
	    if (pthread_create(&threadID_udp1, NULL, ThreadUDP_cl1,
							&broadcast_port_send[i]) != 0)
			DieWithError("pthread_create() failed");	
			
		if (pthread_create(&threadID_udp2, NULL, ThreadUDP_cl2,
							&broadcast_port_recv[i]) != 0)
			DieWithError("pthread_create() failed");	

		broadcast_port_send[i+1] = broadcast_port_send[i] + 1;
		broadcast_port_recv[i+1] = broadcast_port_recv[i] + 1;
	}
		
	console();
	exit(0);
}
