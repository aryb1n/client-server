#include "client.h"

int main(int argc, char *argv[])
{
	char message[MAX_TEXT] = "";  
	int UDP_port[5]; //port for recive udp
	int TCP_Port;
	int j, i = 0;
	for (j = 1, UDP_port[0] = B_PORT_FOR_RECIVER; j < 5; j++)
		UDP_port[j] = UDP_port[j-1] + 1;
	
    for(;;)
    { 	
		while((TCP_Port = init_UDP(UDP_port[i])) < 0)
		{
			i++;
			if(i == 5)
				i = 0;	
		}
			

		int socktcp;
		if((socktcp = init_tcp(TCP_Port)) < 0)
			continue;
	
		if (recv(socktcp, &message, sizeof(message), 0) < 0)
			perror("recv() failed");
		printf("Msg recive : %s \n", message);	      
		
		close(socktcp);
		
		restore_message(message);
		printf("Sleep: %d sec... Len: %d  Text: %s\n\n", 
				msg_struct.sleep_time, msg_struct.len_text,
				msg_struct.some_text);
		
		sleep(msg_struct.sleep_time);
	}
}
