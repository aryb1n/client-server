
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <pthread.h>    /* for POSIX threads */
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX_TEXT 60
#define MAXNUM_MSG_TAIL 10
#define B_PORT_FOR_SENDER 8801 // - 8805
#define B_PORT_FOR_RECIVER 9901 // - 9905
#define SLEEP_UDP 1
#define NUM_PORTS_UDP 5

void console(void);
void DieWithError(char *errorMessage);  /* Error handling function */
int CreateTCPServerSocket(int mark); /* Create TCP server socket */
int AcceptTCPConnection(int servSock);//Accept TCP connection request
int init_broadcast_server(int port);

void *ThreadTCP_recv(void *arg);
void *ThreadTCP_send(void *arg);           
void *Thread_listner(void *arg);
void *ThreadUDP_cl1(void *arg);
void *ThreadUDP_cl2(void *arg);


struct my_msg_st {
 long int my_msg_type;
 char some_text[MAX_TEXT];
};

struct ThreadArgs {
  int clntSock; // Socket descriptor for client
};


struct thread_argument {
	int number_of_message;
	int port_reciver_tcp;
	int port_sender_tcp;
	int msgid;
}broadcast_arg;
