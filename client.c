
#include "client.h"
//testgit
void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}
//asdasd
int init_UDP(int UDP_port)
{
	int sockudp;                         
    struct sockaddr_in UDPAddr; 
    int TCP_Port;

	if ((sockudp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		perror("socket() failed");	
		return -1;
	}

    /* Construct bind structure */
	memset(&UDPAddr, 0, sizeof(UDPAddr));   /* Zero out structure */
	UDPAddr.sin_family = AF_INET; /* Internet address family */
	UDPAddr.sin_addr.s_addr = htonl(INADDR_ANY);//Any incoming interface
	UDPAddr.sin_port = htons(UDP_port);      /* Broadcast port */	
	
	/* Bind to the broadcast port */
	if (bind(sockudp, (struct sockaddr *)&UDPAddr, sizeof(UDPAddr)) < 0)
	{
		perror("bind() failed");
		return -1;
	}

	/* Receive a port for tcp sending */
	if ((recvfrom(sockudp, &TCP_Port,sizeof(TCP_Port), 0, NULL, 0)) < 0)
	{
		perror("recvfrom() failed");
		return -1;
	}		

	printf("UDP port:[%d] TCP port: [%d]\n", UDP_port, TCP_Port);  
	close(sockudp);

	return TCP_Port;
}

int init_tcp(int ServPort)
{   
	int socktcp;                        
	struct sockaddr_in ServAddr;   
		
	if ((socktcp = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		DieWithError("socket() failed");

	
    /* Construct the server address structure */
	memset(&ServAddr, 0, sizeof(ServAddr));  /* Zero out structure */
	ServAddr.sin_family      = AF_INET;  /* Internet address family */
	ServAddr.sin_addr.s_addr = INADDR_ANY;   /* Server IP address */
	ServAddr.sin_port        = htons(ServPort); /* Server port */
	
    /* Establish the connection to the echo server */
	if (connect(socktcp, (struct sockaddr *) &ServAddr,
				sizeof(ServAddr)) < 0)
		perror("connect() failed");	
		
	return socktcp;
}

int create_message(char *message)
{
    msg_struct.sleep_time = rand()%SLEEP_CLIENT;
    msg_struct.len_text = rand()%(MAX_TEXT-20)+10;
   
    for(int i = 0; i < msg_struct.len_text-1; i++)
    {
		switch (rand()%3) {
			case 0: msg_struct.some_text[i]=(char)(rand()%10+48); break;
			case 1: msg_struct.some_text[i]=(char)(rand()%25+65); break;
			case 2: msg_struct.some_text[i]=(char)(rand()%25+97); break;
		}
	}

    msg_struct.some_text[msg_struct.len_text]='\0';
    msg_struct.len_text = strlen(msg_struct.some_text);
    sprintf(message,"%d#%d#%s",msg_struct.sleep_time,
			msg_struct.len_text, msg_struct.some_text);
    
    return 0;
}

int restore_message(char *message){
    int i = 0;
    int j = 0;
    char buf[10];

    for(j = 0; i < strlen(message); i++, j++)
        if(message[i] != '#')
            buf[j] = message[i];
        else{
            buf[j]='\0';
            msg_struct.sleep_time = atoi(buf);
            i++;
            break;
        }
    for(j = 0; i < strlen(message); i++, j++)
                if(message[i] != '#')
            buf[j] = message[i];
        else{
            buf[j]='\0';
			msg_struct.len_text = atoi(buf);
            i++;
            break;
        }
    strcpy(msg_struct.some_text,message + i);
    msg_struct.some_text[msg_struct.len_text] = '\0';

    return 0;
}
