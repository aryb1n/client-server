

all: Server Client_Sender Client_Reciver


#Server

Server: server.o main_server.o
	gcc -Wall -std=c99 -o Server server.o main_server.o -lpthread

server.o: server.h server.c 
	gcc -Wall -std=c99 -c server.c -o server.o -lpthread
	
main_server.o: server.h server.c main_server.c
	gcc -Wall -std=c99 -c main_server.c -o main_server.o -lpthread

# Client Sender

Client_Sender:  client.o client_sender.o
	gcc -Wall -std=c99 -o Client_Sender client.o \
	client_sender.o -lpthread

client.o:  client.c 
	gcc -Wall -std=c99 -c client.c  -o client.o -lpthread

client_sender.o: client.h client_sender.c
	gcc -Wall -std=c99 -c client_sender.c -o client_sender.o -lpthread

# Client Reciver

Client_Reciver: client_reciver.o client.o
	gcc -Wall -std=c99 -o Client_Reciver client.o \
	client_reciver.o -lpthread

client_reciver.o: client.h client_reciver.c
	gcc -Wall -std=c99 -c client_reciver.c -o client_reciver.o -lpthread

clean:
	-rm -rf *.o Client_Reciver Client_Sender Server

  
