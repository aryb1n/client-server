#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <time.h> 		/*time()*/

//Настройка портов
#define MAX_TEXT 60
#define B_PORT_FOR_SENDER 8801 // - 8805
#define B_PORT_FOR_RECIVER  9901 // - 9905
#define SLEEP_CLIENT 9

struct my_msg {
	int sleep_time;
	int len_text;
	char some_text[MAX_TEXT];
}msg_struct;

void DieWithError(char *errorMessage);
int init_UDP(int UDP_port);
int init_tcp(int port);
int create_message(char *message);
int restore_message(char *message);
