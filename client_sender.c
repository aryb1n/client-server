#include "client.h"

int main(int argc, char *argv[])
{
	
	char message[MAX_TEXT] = "";  
	int UDP_port[5]; //port for recive udp
	int TCP_Port;
	int j;
	
	srand(time(NULL));
	/* init UDP port */
	for (j = 1, UDP_port[0] = B_PORT_FOR_SENDER; j < 5; j++) 
		UDP_port[j] = UDP_port[j-1] + 1;
	int i = 0;
	
    for(;;)
    { 	/* if bind() failed, new port*/
		while((TCP_Port = init_UDP(UDP_port[i])) < 0) 
		{
			i++;
			if(i == 5)
				i = 0;	
		}
			
		create_message(message);	
		
		int socktcp = init_tcp(TCP_Port);
	
		if ((send(socktcp, &message, strlen(message), 0)) < 0 )
			DieWithError("send()");
		printf("Msg send: %s \n", message);	     

		close(socktcp);
		printf("sleep %d sec...\n\n", msg_struct.sleep_time);
		sleep(msg_struct.sleep_time);
	}
}
