#include "server.h"

void *Thread_listner(void *arg)
{	
	int mark = *(int*)arg;
	//pthread_detach(pthread_self()); 
	int servSock;
	int clntSock;		/* Socket descriptor for client */
	pthread_t threadID; 
		
	servSock = CreateTCPServerSocket(mark);
	
	while (1) 
	{
		if ((clntSock = AcceptTCPConnection(servSock)) < 0)
			continue;
		
		// Create separate memory for client argument
		struct ThreadArgs *threadArgs = (struct ThreadArgs *) malloc(
											sizeof(struct ThreadArgs));
		if (threadArgs == NULL)
			DieWithError("malloc() failed");
		threadArgs->clntSock = clntSock;
	
		/* Create client thread */
		if(mark == 0){	//If tcp reciver
			if (pthread_create(&threadID, NULL, ThreadTCP_recv,
								threadArgs) != 0)
				DieWithError("pthread_create() failed");
			//printf("with thread %ld \n", (long int) threadID);
		}
		else{	//If tcp sender
			if (pthread_create(&threadID, NULL, ThreadTCP_send,
								threadArgs) != 0)
				DieWithError("pthread_create() failed");
			//printf("with thread %ld \n", (long int) threadID); 
		}
	}
}

void *ThreadUDP_cl1(void *arg)
{
	int broadcast_port = *(int*)arg;
	struct sockaddr_in broadcastAddr;
	int sockUDP;
	if((sockUDP = init_broadcast_server(broadcast_port)) < 0)
		DieWithError("init_broadcast_server() failed");
		
	/* Construct local address structure */
	memset(&broadcastAddr, 0, sizeof(broadcastAddr));   
	broadcastAddr.sin_family = AF_INET;                 
	broadcastAddr.sin_addr.s_addr =htonl(INADDR_BROADCAST);
	broadcastAddr.sin_port = htons(broadcast_port);     
	

	for (;;) /* Run forever */
	{
		if(broadcast_arg.number_of_message < MAXNUM_MSG_TAIL)
		{
			//printf("UDP1 SEND....\n");
	   		if (sendto(sockUDP, &broadcast_arg.port_reciver_tcp,
						sizeof(broadcast_arg.port_reciver_tcp), 0,
						(struct sockaddr *)&broadcastAddr,
						sizeof(broadcastAddr)) <0)
			{
				perror("sendto()");
				continue;
			}
			sleep(SLEEP_UDP);   
		}
	}
	return 0;
}

void *ThreadUDP_cl2(void *arg)
{
	int broadcast_port = *(int*)arg;
	int sockUDP;
	struct sockaddr_in broadcastAddr;
	if((sockUDP = init_broadcast_server(broadcast_port)) < 0)
		DieWithError("init_broadcast_server() failed");
		
	/* Construct local address structure */
	memset(&broadcastAddr, 0, sizeof(broadcastAddr));  
	broadcastAddr.sin_family = AF_INET;   /* Internet address family */
	broadcastAddr.sin_addr.s_addr =htonl(INADDR_BROADCAST);	
	broadcastAddr.sin_port = htons(broadcast_port);/* Broadcast port */
	
	for (;;) /* Run forever */
	{
		if(broadcast_arg.number_of_message > 0)
		{			
			//printf("UDP2 SEND....\n");
			if (sendto(sockUDP, &broadcast_arg.port_sender_tcp,
					sizeof(broadcast_arg.port_sender_tcp), 0, 
					(struct sockaddr *) &broadcastAddr, 
					sizeof(broadcastAddr)) < 0)
			{
				perror("sendto()");
				continue;
			}
			sleep(SLEEP_UDP);  
		}
	}
	
	return 0;
}

void *ThreadTCP_recv(void *threadArgs)
{	
	int clntSock = ((struct ThreadArgs *) threadArgs)->clntSock;
	free(threadArgs); // Deallocate memory for argument
   
	//pthread_detach(pthread_self());               
	struct my_msg_st some_data;
	some_data.my_msg_type = 1;
	char message[MAX_TEXT] = "";
	int len = sizeof(struct my_msg_st) - sizeof(long int);

   	if (recv(clntSock, &message, sizeof(message), 0) < 0)
   	{
		perror("recv() failed");
		return 0;
	}
	close(clntSock);   
	
	printf("Message recive: %s \n", message);
		
	strcpy(some_data.some_text, message);
	if (msgsnd(broadcast_arg.msgid, &some_data, len, IPC_NOWAIT) < 0)
	{
		perror("msgsnd failed\n");
		return 0;
	}
	else broadcast_arg.number_of_message++;
	
	printf("MSGSND: %s \n", some_data.some_text);
	
	return 0;
}

void *ThreadTCP_send(void *threadArgs)
{
	int clntSock = ((struct ThreadArgs *) threadArgs)->clntSock;
	free(threadArgs); // Deallocate memory for argument
	//pthread_detach(pthread_self()); 
	struct my_msg_st some_data;
	some_data.my_msg_type = 1;
	char message[MAX_TEXT] = "";
	int len = sizeof(struct my_msg_st) - sizeof(long int);
	
	if (msgrcv(broadcast_arg.msgid, (void *)&some_data, len,
				some_data.my_msg_type, IPC_NOWAIT) < 0)
	{
		perror("msgrcv failed with error: %d\n");
		return 0;
	}
	else broadcast_arg.number_of_message--;
	
	printf("MSGRCV: %s \n", some_data.some_text);	
	
	strcpy(message, some_data.some_text);
	if (send(clntSock, (void*)&message, sizeof(message), 0) < 0)
	{
		perror("send() failed");
		return 0;
	}
	printf("Message send: %s \n", message);	    
	
	close(clntSock);    

	return 0;
}

void DieWithError(char *errorMessage)
{
	perror(errorMessage);
	exit(1);
}

int CreateTCPServerSocket(int mark)
{
	int sock;	/* socket to create */
	struct sockaddr_in ServAddr;	/* Local address */

	/* Create socket for incoming connections */
	if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		DieWithError("socket() failed");
	  
	/* Construct local address structure */
	memset(&ServAddr, 0, sizeof(ServAddr));   /* Zero out structure */
	ServAddr.sin_family = AF_INET;	/* Internet address family */
	ServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	ServAddr.sin_port = 0;	/* Local port */
	
	/* Bind to the local address */
	if (bind(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
		DieWithError("bind() failed");

	socklen_t len = sizeof(ServAddr);
	if (getsockname(sock, (struct sockaddr *)&ServAddr, &len) == -1)
		DieWithError("Getsockname");
	
	if(mark == 0){
		broadcast_arg.port_reciver_tcp = (int)ntohs(ServAddr.sin_port);
		printf("Port for reciver: [%d] \n", ntohs(ServAddr.sin_port));
	}
	else{
		broadcast_arg.port_sender_tcp = (int) ntohs(ServAddr.sin_port);
		printf("Port for senders: [%d] \n", ntohs(ServAddr.sin_port));
	}
		
	 /* Mark the socket so it will listen for incoming connections */
	if (listen(sock, 1) < 0)
		perror("listen() failed");
	printf("listen socket!\n");
	
	return sock;
}

int AcceptTCPConnection(int servSock)
{
	int clntSock;                    /* Socket descriptor for client */
	struct sockaddr_in ClntAddr; /* Client address */
	unsigned int clntLen; /* Length of client address data structure */
	
	/* Set the size of the in-out parameter */
	clntLen = sizeof(ClntAddr);
	
	/* Wait for a client to connect */
	if ((clntSock = accept(servSock, (struct sockaddr *) &ClntAddr,
							&clntLen)) < 0)
	{						
		perror("accept() failed");
		return -1;
	}
	return clntSock;
}

int init_broadcast_server(int port)
{
	int sockUDP;                                    
	int broadcastPermission = 1;                

	printf("UDPstart port[%d]\n", port);
	
	/* Create socket for sending/receiving datagrams */
	if ((sockUDP = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		perror("socket() failed");
		return -1;
	}
	/* Set socket to allow broadcast */
	if (setsockopt(sockUDP, SOL_SOCKET, SO_BROADCAST,
		(void *)&broadcastPermission, sizeof(broadcastPermission)) < 0)
	{
		perror("setsockopt() failed");
		return -1;
	}
	
 	return sockUDP;
}   

void console()
{
	printf("\nEnter 'q' from exit!!!\n");
	int c;
	while(1){
		c = getc(stdin);
		switch(c) {
			case 'q'    :
				msgctl(broadcast_arg.msgid, IPC_RMID, NULL);
				printf("Server shut down\n");
				return;
			break;
		}
	}
}
